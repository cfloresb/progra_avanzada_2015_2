""" Pauta laboratorio 1 diagnóstico."""

# En primer lugar creamos el tipo de dato Auto:
class Auto:
    """
    Esta clase define un objeto de tipo Auto, entendido como un objeto con las propiedades
    patete, marca y color:
    """
    def __init__(self, patente, marca, color):
        """
        Constructor de la clase Auto. Patente, marca y color serán variables de tipo string (str)
        """
        self.patente = patente
        self.marca = marca
        self.color = color

# La lógica principal del programa:
def main():
    # Se define una lista vacía donde guardaremos los objetos de tipo auto
    lista_autos = []

    # Se crea el objeto "archivo" de tipo "File", el cual representa el archivo de texto:
    archivo = open('bd_autos.txt', 'r')

    # Luego recorremos el archivo y añadimos a la lista de autos cada uno de los autos que ahi se definen:
    for linea in archivo.readlines():
        # Como la patente, la marca y el color están separados por espacios, los capturamos utilizando split()
        patente, marca, color = linea.split(" ")

        # Una vez capturada la informacion creamos un nuevo objeto de tipo auto:
        nuevo_auto = Auto(patente, marca, color)

        # Agregamos el nuevo objeto creado a nuestra lista:
        lista_autos.append(nuevo_auto)

    # Una vez terminado el for, toda la información de los autos está en nuestra lista. Preguntemos al usuario qué
    # quiere buscar:
    patente_a_buscar = input("Ingrese la patente a buscar: ")

    # Y ahora buscarmos la información:
    for auto in lista_autos:
        if auto.patente == patente_a_buscar:
            print("Vehículo encontrado.")
            print("Patente: {}, Marca: {}, Color: {}".format(auto.patente, auto.marca, auto.color))
            return

    # Si llegamos al final del for quiere decir que recorrimos todos los autos y no encontramos la patente:
    print("Vehículo no encontrado")

if __name__ == '__main__':
    # Sólo si el script es ejecutado como el script principal, invocamos la función main()
    main()


