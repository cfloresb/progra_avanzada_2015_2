"""
Este archivo crea una base de datos de autos, como la utilizada en el enunciado del 1er laboratorio 2sem 2015:
"""

import random

colores = ['negro', 'blanco', 'rojo', 'azul', 'gris', 'plateado', 'burdeo', 'beige', 'celeste', 'amarillo']

marcas = ['Toyota', 'Nissan', 'Mercedes-Benz', 'BMW', 'Audi', 'Hyundai', 'Peugeot', 'Renault', 'Citroen',
          'Chevrolet', 'Ford', 'Volkswagen', 'Jaguar', 'Volvo', 'Opel', 'Mitsubishi', 'Honda']

letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

patentes = set()
for i in range(10000):
    patente = ''
    for j in range(4):
        patente += (random.choice(letras))
    patente += str(random.randint(0,9))
    patente += str(random.randint(0,9))
    patentes.add(patente)

with open('bd_autos.txt', 'w') as file:
    for patente in patentes:
        file.write('{} {} {}\n'.format(patente, random.choice(marcas), random.choice(colores)))

