__author__ = 'camilo'

from math import sqrt, atan, pi
from time import sleep

class Punto:
    """
    Esta clase representará un punto en el plano de dos dimensiones.
    Ejemplos de cosas que debería hacer:
    >>> p1 = Punto()
    >>> p1.x
    0
    >>> p1.y
    0
    >>> p2 = Punto(3,4)
    >>> p1.imprimir()
    (0 , 0)
    >>> p1.calcular_distancia(p2)
    5.0
    """
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def imprimir(self):
        print("({} , {})".format(self.x, self.y))
    def calcular_distancia(self, otro_punto):
        return sqrt((self.x-otro_punto.x)**2+(self.y-otro_punto.y)**2)


class Vector:
    """
    Esta clase representará un vector con dos coordenadas.
    Ejemplo:
    >>> v1 = Vector(3,4)
    >>> v1.x
    3
    >>> v1.calcular_norma()
    5.0
    >>> v1.calcular_angulo()
    53.13010235415598
    """
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def calcular_norma(self):
        return sqrt(self.x**2 + self.y**2)
    def calcular_angulo(self):
        return 180*atan(self.y/self.x)/pi


class Objetivo:
    """
    Esta es una clase muy sencilla que representará un objetivo (un círculo en el plano)
    Ejemplo:
    >>> p1 = Punto(1,2)
    >>> objetivo = Objetivo(p1, 3)
    >>> objetivo.posicion.x
    1
    >>> objetivo.radio
    3
    """
    def __init__(self, posicion, radio):
        self.posicion = posicion
        self.radio = radio


class Proyectil:
    """
    Esta clase representará un proyectil en el juego. Ejemplo:
    >>> p = Punto(1,2)
    >>> v = Vector(3,4)
    >>> mi_proyectil = Proyectil(p, v)
    >>> mi_proyectil.posicion_inicial.x
    1
    >>> mi_proyectil.posicion_inicial.y
    2
    >>> mi_proyectil.calcular_velocidad()
    5.0
    """
    def __init__(self, posicion_inicial, velocidad_inicial):
        self.posicion_inicial = posicion_inicial
        self.velocidad_inicial = velocidad_inicial
    def calcular_velocidad(self):
        return self.velocidad_inicial.calcular_norma()


class Juego:
    """
    Esta clase representa el juego y contiene toda la lógica para calcular
    la posición de los elementos dentro de él y si han habido colisiones o no.
    Ejemplo:
    >>> lanzamiento1 = Proyectil(posicion_inicial=Punto(0,0), velocidad_inicial=Vector(1,10))
    >>> ob1 = Objetivo(Punto(2,3), 1)
    >>> juego = Juego(lanzamiento1, ob1, 0.1)
    >>> juego.t # Los objetos tipo juego tienen el atributo t que representa al tiempo. Al crear un juego, t debe ser 0
    0
    >>> posicion_proyectil0 = juego.calcular_posicion_proyectil() # Esto debe ser un objeto de tipo Punto
    >>> int(posicion_proyectil0.x)
    0
    >>> int(posicion_proyectil0.y)
    0
    >>> juego.t = 1  # Podemos hacer avanzar el tiempo del juego "a mano"
    >>> posicion_proyectil1 = juego.calcular_posicion_proyectil()
    >>> posicion_proyectil1.x
    1
    >>> posicion_proyectil1.y
    5.1
    >>> juego.proyectil_en_objetivo()
    False
    >>> juego.t = 0 # volvamos atrás en el tiempo
    >>> juego.ejecutar() # Ejecutamos el juego y debería aparecer la siguiente info en pantalla:
    Velocidad inicial del proyectil: 10.05 mts/seg
    El ángulo de salida del proyectil es de: 84.29 grados
    t=0.00 : (0.00, 0.00)
    t=0.10 : (0.10, 0.95)
    t=0.20 : (0.20, 1.80)
    t=0.30 : (0.30, 2.56)
    t=0.40 : (0.40, 3.22)
    t=0.50 : (0.50, 3.77)
    t=0.60 : (0.60, 4.24)
    t=0.70 : (0.70, 4.60)
    t=0.80 : (0.80, 4.86)
    t=0.90 : (0.90, 5.03)
    t=1.00 : (1.00, 5.10)
    t=1.10 : (1.10, 5.07)
    t=1.20 : (1.20, 4.94)
    t=1.30 : (1.30, 4.72)
    t=1.40 : (1.40, 4.40)
    t=1.50 : (1.50, 3.97)
    t=1.60 : (1.60, 3.46)
    POW!!! El proyectil ha dado en el objetivo
    Distancia aproximada recorrida por el proyectil : 7.07 metros


    """
    def __init__(self, proyectil, objetivo, delta_t=0.1):
        self.proyectil = proyectil
        self.objetivo = objetivo
        self.delta_t = delta_t
        self.t = 0

    def calcular_posicion_proyectil(self):
        x_t = self.proyectil.posicion_inicial.x + self.proyectil.velocidad_inicial.x*self.t
        y_t = self.proyectil.posicion_inicial.y + self.proyectil.velocidad_inicial.y*self.t - 1/2*9.8*self.t**2
        return Punto(x_t, y_t)

    def proyectil_en_objetivo(self):
        posicion_actual = self.calcular_posicion_proyectil()
        return self.objetivo.posicion.calcular_distancia(posicion_actual) <= self.objetivo.radio

    def ejecutar(self):
        trayectoria = []

        print("Velocidad inicial del proyectil: {:.2f} mts/seg".format(self.proyectil.velocidad_inicial.calcular_norma()))
        print("El ángulo de salida del proyectil es de: {:.2f} grados".format(self.proyectil.velocidad_inicial.calcular_angulo()))
        while True:
            posicion_proyectil = self.calcular_posicion_proyectil()
            trayectoria.append(posicion_proyectil)
            print("t={:.2f} : ({:.2f}, {:.2f})".format(self.t, posicion_proyectil.x, posicion_proyectil.y))

            if self.proyectil_en_objetivo():
                print("POW!!! El proyectil ha dado en el objetivo")
                break
            elif self.t!= 0 and posicion_proyectil.y <=0:
                print("POW!!!! Tu proyectil ha caido en el piso sin dar en el objetivo!")
                break

            sleep(self.delta_t)
            self.t = self.t+self.delta_t

        distancia_recorrida = 0
        punto_anterior = self.proyectil.posicion_inicial
        for punto in trayectoria:
            distancia_recorrida += punto.calcular_distancia(punto_anterior)
            punto_anterior = punto

        print("Distancia aproximada recorrida por el proyectil : {:.2f} metros".format(distancia_recorrida))

if __name__ == '__main__':
    import doctest
    doctest.testmod()