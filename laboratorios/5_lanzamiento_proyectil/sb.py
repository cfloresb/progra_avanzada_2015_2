__author__ = 'camilo'

from math import sqrt
class Punto:
    """
    Esta clase representará un punto en el plano de dos dimensiones.
    Ejemplos de cosas que debería hacer:
    >>> p1 = Punto()
    >>> p1.x
    0
    >>> p1.y
    0
    >>> p2 = Punto(3,4)
    >>> p1.imprimir()
    (0 , 0)
    >>> p1.calcular_distancia(p2)
    5.0
    """
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def imprimir(self):
        print("({} , {})".format(self.x, self.y))
    def calcular_distancia(self, otro_punto):
        return sqrt((self.x-otro_punto.x)**2+(self.y-otro_punto.y)**2)

class PosicionEstatica:
     def __init__(self):
         self.x=2
         self.y=0

p1 = Punto(2,3)
p2 = Punto(3,1)

pe = PosicionEstatica()

p1.calcular_distancia(p2)

p1.calcular_distancia(pe)

