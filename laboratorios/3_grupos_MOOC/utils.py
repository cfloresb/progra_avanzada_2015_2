__author__ = 'camilo'

def leer_archivo(nombre_archivo):
        with open(nombre_archivo, 'r', encoding='utf-8') as archivo:
            return [nombre.strip('\n') for nombre in archivo.readlines()]



def escribir_grupos(lista_grupos, nombre_archivo):
    with open(nombre_archivo, 'w', encoding='utf-8') as archivo:
        for indice, grupo in enumerate(lista_grupos):
            if len(grupo) < 3:
                raise Exception("Error. Recuerda que no pueden haber grupos de menos de 3 personas")
            elif len(grupo) == 3:
                archivo.write("grupo_{}, {}, {}, {}\n".format(indice, grupo[0], grupo[1], grupo[2]))
            elif len(grupo) == 4:
                archivo.write("grupo_{}, {}, {}, {}, {}\n".format(indice, grupo[0], grupo[1], grupo[2], grupo[3]))
            else:
                raise Exception("Error. No pueden haber grupos de más de 4 alumnos.")


