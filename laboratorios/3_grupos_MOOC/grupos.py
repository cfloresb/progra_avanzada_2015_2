__author__ = 'camilo'

from utils import leer_archivo, escribir_grupos

programacion = leer_archivo('programacion.txt')
mecanica = leer_archivo('mecanica.txt')
calculo = leer_archivo('calculo.txt')

programacion = set(programacion)
mecanica = set(mecanica)
calculo = set(calculo)


print("La cantidad de alumnos en programacion es de {}".format(len(programacion)))
print("La cantidad de alumnos en calculo es de {}".format(len(calculo)))
print("La cantidad de alumnos en mecanica es de {}".format(len(mecanica)))

en_los_tres = programacion & mecanica & calculo
print("La cantidad de alumnos en los tres ramos simultaneamente es de {}".format(len(en_los_tres)))

programacion_mecanica = programacion & mecanica - en_los_tres
programacion_calculo = programacion & calculo - en_los_tres
mecanica_calculo = mecanica & calculo - en_los_tres

print("La cantidad de alumnos en programacion y mecanica es ", len(programacion_mecanica))
print("La cantidad de alumnos en programacion y calculo es ", len(programacion_calculo))
print("La cantidad de alumnos en mecanica y calculo es ", len(mecanica_calculo))

solo_programacion = programacion - mecanica - calculo
solo_mecanica = mecanica - programacion - calculo
solo_calculo = calculo - programacion - mecanica

print("La cantidad de alumnos sólo en programacion es ", len(solo_programacion))
print("La cantidad de alumnos sólo en mecanica es ", len(mecanica))
print("La cantidad de alumnos sólo en calculo es ", len(calculo))


grupos_en_los_tres = []
for i in range(0, len(en_los_tres) % 3):
    grupos_en_los_tres.append([en_los_tres.pop(), en_los_tres.pop(), en_los_tres.pop(), en_los_tres.pop()])

while len(en_los_tres) > 0:
    grupos_en_los_tres.append([en_los_tres.pop(), en_los_tres.pop(), en_los_tres.pop()])

escribir_grupos(grupos_en_los_tres, 'en_los_tres.txt')

grupos_en_dos = []
el_mayor = max(programacion_calculo, programacion_mecanica, mecanica_calculo)
for i in range(0, min(len(programacion_calculo), len(programacion_mecanica), len(mecanica_calculo)) % 3):
    grupos_en_dos.append([programacion_calculo.pop(), programacion_mecanica.pop(), mecanica_calculo.pop(), el_mayor.pop()])

while len(programacion_calculo) >0 and len(programacion_mecanica) >0 and len(mecanica_calculo)>0:
    grupos_en_dos.append([programacion_calculo.pop(), programacion_mecanica.pop(), mecanica_calculo.pop()])

if len(programacion_calculo) >0 or len(programacion_mecanica) >0 or len(mecanica_calculo)>0:
    print("Quedaron alumnos participando en dos cursos sin grupo!")

escribir_grupos(grupos_en_dos, 'en_dos.txt')

grupos_en_uno = []
el_mayor = max(solo_programacion, solo_calculo, solo_mecanica)
for i in range(0, min(len(solo_programacion), len(solo_mecanica), len(solo_calculo)) % 3):
    grupos_en_uno.append([solo_programacion.pop(), solo_programacion.pop(), solo_mecanica.pop(), el_mayor.pop()])

while len(solo_programacion) >0 and len(solo_mecanica) >0 and len(solo_calculo)>0:
    grupos_en_uno.append([solo_programacion.pop(), solo_programacion.pop(), solo_mecanica.pop()])

if len(programacion_calculo) >0 or len(programacion_mecanica) >0 or len(mecanica_calculo)>0:
    print("Quedaron alumnos participando en un sólo curso sin grupo!")

escribir_grupos(grupos_en_uno, 'en_uno.txt')


