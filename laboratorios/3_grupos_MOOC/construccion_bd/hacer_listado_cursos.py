__author__ = 'camilo'

#! /bin/python3
import random

archivo_nombres = 'nombres.txt'
archivo_apellidos = 'apellidos.txt'

nombres, apellidos = [], []

file = open(archivo_nombres, 'r')
for linea in file.readlines():
    nombres.append(linea.strip('\n'))

file.close()

file = open(archivo_apellidos, 'r')
for linea in file.readlines():
    apellidos.append(linea.strip('\n'))

file.close()

for curso in ('programacion', 'calculo', 'mecanica'):
    file = open(curso+'.txt', 'w')

    i = 0
    max_number = random.randint(600000, 800000)

    while i< max_number:
        file.write(random.choice(nombres) + ' ' + random.choice(apellidos) + '\n')
        i+=1

    file.close()
