# README #

Este repositorio contiene código Python utilizado en las clases de Programación Avanzada de la Universidad Finis Terrae.

### Organización del código ###

* Ejemplos: Carpeta con snippets breves de código sencillos. Generalmente vienen autoexplicados (estamos trabajando para ud.)
* Laboratorios: En esta carpeta se almacenan las pautas propuestas para los laboratorios. Cada carpeta tiene el código de la pauta propuesta, así como el enunciado en formato PDF, de manera de dar más contexto

### Contribuciones ###

* Si quieres contribuir es muy bienvenido cualquier aporte, ya sea a través de reportar un bug (incidencias) o derechamente solicitar un pull request.
