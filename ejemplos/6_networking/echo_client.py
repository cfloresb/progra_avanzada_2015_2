"""
Adaptado desde https://docs.python.org/3.4/library/socket.html
"""

import socket

HOST = '127.0.0.1'    # Dirección del host remoto
PORT = 5101           # Puerto donde está escuchando el proceso remoto

# Creamos el objeto que representa el socket (IPv4, TCP)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Intentamos conectarnos al servidor:
s.connect((HOST, PORT))
try:
    while True:
        texto = input("Ingrese el texto a enviar: ")
        # Enviamos el texto convertido a bytes:
        s.sendall(texto.encode())
        data = s.recv(1024)
        print('Recibido:', data.decode())
except (KeyboardInterrupt, EOFError):
    print("\nPrograma cerrado por el usuario. Liberando recursos...")
    s.close()
