"""
Adaptado desde https://docs.python.org/3.4/library/socket.html
"""
import socket

HOST = '127.0.0.1' # Escucharemos la dirección local
PORT = 5101        # Puerto sin privilegios (>1000) arbitrario

# Creamos el objeto socket de tipo (IPv4, TCP)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Conectamos el socket a la tupla dirección, puerto que queremos escuchar:
s.bind((HOST, PORT))
# Configuramos el socket para escuchar 1 conexión
s.listen(1)

while True:
    try:
        # Quedamos a la espera de conexiones:
        print("Esperando conexiones...")
        conn, addr = s.accept()
        print('Conectado por:', addr)
        while True:
            # Quedamos a la espera de pedazos de a 1024 bytes
            data = conn.recv(1024)
            if not data:
                # Si no llegan datos la conexión de rompió y por lo tanto debemos cerrarla
                print("Conexión cerrada por el cliente.")
                conn.close()
                break
            else:
                # Imprimimos la información recibida y la devolvemos:
                print(data.decode())
                conn.sendall(data)
    except (KeyboardInterrupt, EOFError):
        print("Ejecución cancelada por el usuario.")
        break
