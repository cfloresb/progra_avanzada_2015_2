'''
Created on 09-10-2014

@author: camilo
'''
def es_primo(numero):
    
    es_primo = True
    
    for n in range(2,numero):   #Se puede hacer más eficiente el programa haciendo "for n in range(2,numero//2+1)"
        if numero%n == 0:
            es_primo = False
            break
    
    return es_primo