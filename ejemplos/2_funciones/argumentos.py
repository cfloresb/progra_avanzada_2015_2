'''
Created on 09-10-2014

@author: camilo
'''

def imprime_todo_posicional(*args):
    for cuenta, cosa in enumerate(args):
        print(str(cuenta), ".", cosa)

def imprime_todo_nominados(**kwargs):
    for nombre, valor in kwargs.items():
        print(nombre, ".", valor)
