'''
Created on 09-10-2014

@author: camilo
'''

def factorial(numero):
    total = 1
    
    for i in range(1,numero+1):
        total = total * i
    
    return total