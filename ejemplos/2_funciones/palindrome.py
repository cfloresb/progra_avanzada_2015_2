'''
Created on 09-10-2014

@author: camilo
'''

def palindrome(palabra):
    palabra2 = palabra[::-1]
    i = 0
    for letra in palabra:
        if palabra2[i] != letra:
            return False
        i += 1
    return True