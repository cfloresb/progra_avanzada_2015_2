'''
Created on 09-10-2014

@author: camilo
'''

def factorial_rec(numero):
    if numero == 1:
        return 1

    else:
        n_factorial = numero*factorial_rec(numero-1)
        return n_factorial