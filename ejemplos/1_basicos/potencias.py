'''Este programa calcula hasta la n-esima potencia de un número escribiendo el resultado
en un archivo'''

base = int(input("Ingrese la base: "))
exponente = int(input("Ingrese hasta qué exponente calcular: "))
nombre_archivo = input("Ingrese el nombre del archivo a guardar: ")

f = open(nombre_archivo, 'x')

i = 0

while i < exponente:
    f.write(str(base) + " elevado a "+str(i)+" es igual a: "+ str(base**i)+"\n")
    i += 1

f.close()
print("Programa finalizado.")

