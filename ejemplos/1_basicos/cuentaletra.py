'''Este programa cuenta la veces que aparece una letra en un texto'''

texto = input("Ingrese el texto a revisar: ")

letra_a_buscar = input("Ingrese la letra a contar: ")

contador = 0
for letra in texto:
    if letra == letra_a_buscar:
        contador = contador + 1

print("Se encontró", contador, "veces la letra ingresada en el texto")



