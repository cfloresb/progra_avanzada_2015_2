'''Este programa solicita al usuario ingresar la temperatura del agua en 
grados centígrados y entrega el estado (sólido, líquido, gaseoso) en que
se encuentra'''

temperatura = float(input("Ingresa la temperatura del agua: "))
if temperatura < 0:
    print("El estado es sólido = hielo. Prepárate una bebida!")
elif temperatura < 100:
    print("El estado es líquido. Puedes tomarte el agua")
else:
    print("El estado es gaseoso. Cuidado con quemarte!")

