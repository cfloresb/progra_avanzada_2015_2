'''Al importar este archivo cómo módulo sólo se definirá la función factorial'''
def factorial(numero):
    total = 1
    
    for i in range(1,numero+1):
        total = total * i
    
    return total

if __name__ == '__main__':
    '''Este trozo de código sólo se ejecutará si el archivo es ejecutado como
    el script principal (sólo en ese caso el valor del namespace es __main__'''
    numero = int(input('ingrese el número: '))
    print('el resultado es',factorial(numero))
