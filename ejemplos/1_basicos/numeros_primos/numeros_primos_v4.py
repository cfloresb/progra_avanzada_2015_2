# Empaquetamos la lógica de análisis de número primo en una función:
def es_primo(numero):
    for divisor in range(2,numero):
        if (numero % divisor) == 0:
            return False
    return True

# Ahora va la lógica principal del programa:

# Primero pedimos al usurio el número a analizar:
numero_a_analizar = int(input("Ingrese el número a analizar: "))

# Ahora tenemos que analizar todos los números hasta el ingresado por el usuario
# imprimiendo aquellos que sean primos:
for numero in range(1,numero_a_analizar+1):
    if es_primo(numero):
        print(numero)