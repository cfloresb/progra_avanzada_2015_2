# Primero pedimos al usurio el número a analizar:
numero_a_analizar = int(input("Ingrese el número a analizar: "))

# Luego buscamos e imprimimos todos los primos entre 1 y el número,
# incluyendo a ambos:
for numero in range(1, numero_a_analizar+1):
    es_primo = True
    for divisor in range(2,numero):
        if (numero % divisor) == 0:
            es_primo = False
            break
    if es_primo:
        print(numero)