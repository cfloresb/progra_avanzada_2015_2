# Primero pedimos al usurio el número a analizar:
numero_a_analizar = int(input("Ingrese el número a analizar: "))

# Luego analizamos si el número es primo o no:
es_primo = True
for divisor in range(2,numero_a_analizar):
    if (numero_a_analizar % divisor) == 0:
        es_primo = False

# Avisamos al usuario el resultado de nuestro cálculo:
if es_primo:
    print("El número es primo")
else:
    print("El número no es primo")