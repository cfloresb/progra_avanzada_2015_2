# Empaquetamos la lógica de análisis de número primo en una función:
def es_primo(numero):
    for divisor in range(2,numero):
        if (numero % divisor) == 0:
            return False
    return True

# Empaquetamos la lógica de impresión de números primos:
def imprimir_primos(numero_hasta):
    for numero in range(1,numero_hasta+1):
        if es_primo(numero):
            print(numero)

# Ahora va la lógica principal del programa:

# Primero pedimos al usurio el número a analizar:
numero_a_analizar = int(input("Ingrese el número a analizar: "))
# Imprimimos los números primos correspondientes:
imprimir_primos(numero_a_analizar)