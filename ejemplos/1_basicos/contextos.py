def suma1(numero):
    numero += 1
    return numero

def elevar_al_cuadrado(numero):
    numero **= 2
    return numero

numero = int(input("Ingrese el número a operar: "))

numero1 = suma1(elevar_al_cuadrado(numero))
numero2 = elevar_al_cuadrado(suma1(numero))