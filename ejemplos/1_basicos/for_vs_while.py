#Supongamos una lista de números sencilla:
lista = [1,2,3,4]
contador = 0

'''Con el siguiente while recorremos la lista,
 aumentando en 1 cada uno de sus elementos'''
while contador < len(lista):
    lista[contador] = lista[contador] + 1
    contador += 1
    
'''El código anterior es equivalente a lo siguiente'''
lista = [1,2,3,4]
contador = 0
for elemento in lista:
    elemento += 1