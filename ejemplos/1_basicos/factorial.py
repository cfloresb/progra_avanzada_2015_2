''' Al importar este modulo se definirá la función factorial'''
def factorial(numero):
    total = 1
    
    for i in range(1, numero + 1):
        total = total * i
    
    return total

''' Gracias al siguiente if, sólo si el programa es ejecutado
como script principal (el nombre del namespace = __main__)'''
if __name__ == '__main__':
    
    numero = int(input('ingrese el número: '))
    print('el resultado es', factorial(numero))
