'''Este programa pregunta al usuario dos números y calcula el promedio de ambos'''


print("Este programa calcula el promedio de dos números.")
#Se asgina a la variable 'a' la conversión a número entero del string ingresado por el usuario
a = int(input("Por favor, ingrese el primer número: ")) 

#Hagamos lo mismo para la variable 'b':
b = int(input("Por favor, ingrese el segundo número: "))

#En la variable 'promedio' se guarda el resultado del cálculo 
promedio = (a+b)/2

#Finalmente imprimimos el resultado:
print("El promedio de los números ingresados es: ", promedio)
