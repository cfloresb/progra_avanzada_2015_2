'''Este programa calcula hasta el n-esimo término de la sucesión de números de Fibonacci'''

term_max = int(input("Ingrese hasta qué término de la sucesión desea calcular: "))

n = 0
fn = 0
fn1 = 1
while n < term_max:
    print("Término ", n + 1, " = ", fn1)
    temp = fn1
    fn1 = fn + fn1
    fn = temp
    n = n + 1
print("Listo!")
