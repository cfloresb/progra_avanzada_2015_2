"""
Ejemplo básico de una aplicación tkinter OOP.
"""

# 1: Se importa el módulo
import tkinter

# 2: Se crea una nueva clase que hereda de una ventana o un widget sobre
# el cual queremos agregar controles 
class VentanaHolaMundo(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)   # Se invoca al constructor de la clase padre

        # Creamos un widget y lo insertamos dentro del mismo objeto VentanaTkinter
        self.texto = tkinter.Label(master=self, text='Hola curso')
        self.texto.pack()

        # Llamamos el método mainloop
        self.mainloop()


app = VentanaHolaMundo()



