"""
Ejemplo básico de una aplicación tkinter, introduciendo botones y organizando objetos con Frame
"""
import tkinter


class Ejemplo2(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
        #  Crearemos un tipo de control especial, de tipo Frame (marco), donde agruparemos los botones.
        #  Como todos los objetos tkinter, es necesario crearlo pasando como parámetro el objeto que lo contendrá.
        marco = tkinter.Frame(master=self)

        #  Creamos un par de botones. Notar que es posible configurar distintas opciones al crear los objetos.
        boton1 = tkinter.Button(master=marco, text='Botón 1')
        boton2 = tkinter.Button(master=marco, text='Botón 2')
        etiqueta = tkinter.Label(master=self, text='Esta es una etiqueta de texto')

        # Luego, es necesario "empaquetar" los objetos creados invocando al método pack().
        # Es interesante notar que cada objeto debe empaquetarse dentro de su padre (botones->marco; marco->ventana, etc)
        # Es posible pasar además opciones para configurar la disposición de los objetos dentro del marco
        boton1.pack(side=tkinter.LEFT, padx=20, pady=10)  # pad = margen entre el botón y otros controles
        boton2.pack(side=tkinter.LEFT, padx=20, pady=10)
        marco.pack()
        etiqueta.pack()

        # Finalmente ejecutamos el loop de eventos principal:
        self.mainloop()


app = Ejemplo2()
