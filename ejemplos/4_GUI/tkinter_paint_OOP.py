from tkinter import *

class PaintV1(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.title("Paint usando ovals")
        self.canvas = Canvas(self, width=500, height=150, background='#FFFFFF')
        self.canvas.pack(expand=YES, fill=BOTH)
        self.canvas.bind("<B1-Motion>", self.pintar)

        mensaje = Label(self, text="Haz click y arrastra el mouse para dibujar")
        mensaje.pack(side=BOTTOM)

        self.mainloop()

    def pintar(self, evento):
        color = "#FF0000"
        x1, y1 = (evento.x - 1 ), (evento.y - 1)
        x2, y2 = (evento.x + 1), (evento.y + 1)
        self.canvas.create_oval(x1, y1, x2, y2, fill=color, outline=color)


app = PaintV1()
__author__ = 'programacion'
