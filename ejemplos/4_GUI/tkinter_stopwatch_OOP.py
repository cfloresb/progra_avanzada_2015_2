# Adaptado desde  http://code.activestate.com/recipes/124894/

from tkinter import *
import time

class StopWatch(Tk):
    """ Implements a stop watch"""
    def __init__(self):
        Tk.__init__(self)
        self.title('StopWatch App')
        self._start = 0.0
        self._elapsedtime = 0.0
        self._running = 0
        self.timestr = StringVar()
        l = Label(master=self, textvariable=self.timestr)
        self._set_time(self._elapsedtime)
        l.pack(fill=X, expand=NO, pady=2, padx=2)

        Button(master=self, text='Start', command=self.start).pack(side=LEFT)
        Button(master=self, text='Stop', command=self.stop).pack(side=LEFT)
        Button(master=self, text='Reset', command=self.reset).pack(side=LEFT)
        Button(master=self, text='Quit', command=self.quit).pack(side=LEFT)

        self.mainloop()

    def _update(self):
        """ Update the label with elapsed time. """
        self._elapsedtime = time.time() - self._start
        self._set_time(self._elapsedtime)
        self._timer = self.after(1, self._update)

    def _set_time(self, elap):
        """ Set the time string to Minutes:Seconds:Hundreths """
        minutes = int(elap/60)
        seconds = int(elap - minutes*60.0)
        hseconds = int((elap - minutes*60.0 - seconds)*100)
        self.timestr.set(str(minutes)+":"+str(seconds)+":"+str(hseconds))

    def start(self):
        """ Start the stopwatch, ignore if running. """
        if not self._running:
            self._start = time.time() - self._elapsedtime
            self._update()
            self._running = 1

    def stop(self):
        """ Stop the stopwatch, ignore if stopped. """
        if self._running:
            self.after_cancel(self._timer)
            self._elapsedtime = time.time() - self._start
            self._set_time(self._elapsedtime)
            self._running = 0

    def reset(self):
        """ Reset the stopwatch. """
        self._start = time.time()
        self._elapsedtime = 0.0
        self._set_time(self._elapsedtime)


stop_watch_app = StopWatch()