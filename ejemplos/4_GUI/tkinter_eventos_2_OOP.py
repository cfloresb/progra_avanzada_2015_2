""" Codigo adaptado desde http://www.java2s.com/Tutorial/Python/0360__Tkinker/Mouseeventsexample.htm"""

from tkinter import YES, BOTH, BOTTOM, Frame, Label, Tk


class AppConEventos2(Tk):
    def __init__(self):
        Tk.__init__(self)

        self.title = "Ejemplos eventos mouse"
        self.geometry("275x100")
        widget_ubicacion = Frame(master=self)
        widget_ubicacion.pack(expand=YES, fill=BOTH)

        self.posicion_mouse = Label(self)
        self.posicion_mouse.pack(side=BOTTOM)

        self.bind("<Button-1>", self.boton_presionado)
        self.bind("<ButtonRelease-1>", self.boton_liberado)
        self.bind("<Enter>", self.entro_ventana)
        self.bind("<Leave>", self.salio_ventana)
        self.bind("<B1-Motion>", self.mouse_arrastrado)

        self.mainloop()

    def boton_presionado(self, event):
        self.posicion_mouse['text'] = "Botón presionado en [ " + str(event.x) + ", " + str(event.y) + " ]"

    def boton_liberado(self, event):
        self.posicion_mouse['text'] = "Botón liberado en [ " + str(event.x) + ", " + str(event.y) + " ]"

    def entro_ventana(self, event):
        self.posicion_mouse['text'] = "Mouse dentro del widget"

    def salio_ventana(self, event):
        self.posicion_mouse['text'] = "Mouse fuera del widget"

    def mouse_arrastrado(self, event):
        self.posicion_mouse['text'] = "Mouse arrastrado en [ " + str(event.x) + ", " + str(event.y) + " ]"

app = AppConEventos2()
