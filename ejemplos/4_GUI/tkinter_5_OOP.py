"""
Ejemplo de acciones gatilladas por botones
"""
import tkinter


class Ejemplo5(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
    
        # Creamos el marco, donde agruparemos los botones.
        # Como todos los objetos tkinter, es necesario crearlo pasando el objeto que lo 
        # contendrá al constructor de la clase.
        marco = tkinter.Frame(master=self)
        
        # Creamos los controles de nuestro formulario
        etiqueta_nombres = tkinter.Label(master=marco, text='Nombres')
        nombres = tkinter.StringVar()
        campo_nombres = tkinter.Entry(master=marco, textvariable=nombres)
        
        etiqueta_apellidos = tkinter.Label(master=marco, text='Apellidos')
        apellidos = tkinter.StringVar()
        campo_apellidos = tkinter.Entry(master=marco, textvariable=apellidos)
        
        # Para agregar comportamiento a un botón, basta pasar la función como parámetro 'command' al crear el objeto.
        # Notar que se pasa la función como objeto sin invocarla (luego del nombre de la función no hay paréntesis)
        # Además, creamos estos objetos como atributos de la clase de manera de poder acceder a ellos de manera
        # sencilla desde los demás métodos de la clase
        self.boton = tkinter.Button(master=self, text='Cambiar color', command=self.cambiar_color)
        self.cuadro = tkinter.Label(master=self, background='#000000', height=20)

        # Empaquetamos los objetos en una grilla a través del método grid():
        etiqueta_nombres.grid(row=0, column=0)
        campo_nombres.grid(row=0, column=1)
        etiqueta_apellidos.grid(row=1, column=0)
        campo_apellidos.grid(row=1, column=1)
        
        # Empaquetamos los objetos principales dentro de nuestra aplicación
        marco.pack()
        self.boton.pack()
        self.cuadro.pack(fill=tkinter.BOTH)
        
        # Es posible configurar el tamaño de los controles (Ancho x Alto).
        # En este caso configuramos las dimensiones de la self y más atributos
        self.geometry('300x200')
        self.title('Formulario básico de ejemplo')
        self.minsize(width=200, height=150)
        
        # El loop de eventos principal
        self.mainloop()
    
    def cambiar_color(self):
        self.cuadro['background'] = '#FF0000'
        self.boton['text'] = 'Boton ya apretado'
        self.boton['background'] = '#0000FF'
    
app = Ejemplo5()
