"""Ejemplo básico de aplicación con Canvas (dibujo de figuras en pantalla)"""

import tkinter


class AppConCanvas(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
        self.title('Ejemplo uso widget Canvas')

        canvas = tkinter.Canvas(master=self)

        canvas['width'] = 500
        canvas['heigh'] = 300

        canvas.pack()

        canvas.create_line(0, 0, 0, 10, 10, 10, 20, 50, 50, 250, fill='#ABAB00', width=5, smooth=True)
        canvas.create_polygon(40, 23, 213, 34, 124, 34, 234, 300, fill='#00DBDB')
        canvas.create_oval(50, 50, 100, 100, width=2)

        self.mainloop()


app = AppConCanvas()