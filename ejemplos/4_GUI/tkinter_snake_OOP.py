# snake8.py

# http://www.kosbie.net/cmu/fall-10/15-110/handouts/snake/snake-src/snake8.py

import random
from tkinter import *

class SnakeGame(Tk):
    def __init__(self):
        Tk.__init__(self)

        margin = 5
        cell_size = 30

        cols, rows = 16,16

        canvas_width = 2*margin + cols*cell_size
        canvas_height = 2*margin + rows*cell_size

        self.canvas = Canvas(master=self, width=canvas_width, height=canvas_height)
        self.canvas.pack()
        self.resizable(width=0, height=0)
        # Store canvas in root and in canvas itself for callbacks
        #root.canvas = canvas.canvas = canvas
        # Set up canvas data and call init
        self.canvas.data = { }
        self.canvas.data["margin"] = margin
        self.canvas.data["cellSize"] = cell_size
        self.canvas.data["canvasWidth"] = canvas_width
        self.canvas.data["canvasHeight"] = canvas_height
        self.canvas.data["rows"] = rows
        self.canvas.data["cols"] = cols
        self.init()
        # set up events
        self.bind("<Button-1>", self.redibujar_todo)
        self.bind("<Key>", self.tecla_persionada)
        self.actualizar_cronometro()
        # and launch the app
        self.mainloop()  # This call BLOCKS (so your program waits until you close the window!)


    def tecla_persionada(self, event):

        self.canvas.data["ignoreNextimerEvent"] = True # for better timing
        # first process keys that work even if the game is over
        if (event.char == "q"):
            self.game_over()
        elif (event.char == "r"):
            self.init()
        elif (event.char == "d"):
            self.canvas.data["inDebugMode"] = not self.canvas.data["inDebugMode"]
        # now process keys that only work if the game is not over
        if (self.canvas.data["isGameOver"] == False):
            if (event.keysym == "Up"):
                self.mover_snake(-1, 0)
            elif (event.keysym == "Down"):
                self.mover_snake(+1, 0)
            elif (event.keysym == "Left"):
                self.mover_snake(0,-1)
            elif (event.keysym == "Right"):
                self.mover_snake(0,+1)
        self.redibujar_todo()

    def mover_snake(self, drow, dcol):
        # move the snake one step forward in the given direction.
        self.canvas.data["snakeDrow"] = drow # store direction for next timer event
        self.canvas.data["snakeDcol"] = dcol
        snakeBoard = self.canvas.data["snakeBoard"]
        rows = len(snakeBoard)
        cols = len(snakeBoard[0])
        headRow = self.canvas.data["headRow"]
        headCol = self.canvas.data["headCol"]
        newHeadRow = headRow + drow
        newHeadCol = headCol + dcol
        if ((newHeadRow < 0) or (newHeadRow >= rows) or
            (newHeadCol < 0) or (newHeadCol >= cols)):
            # snake ran off the board
            self.game_over()
        elif (snakeBoard[newHeadRow][newHeadCol] > 0):
            # snake ran into itself!
            self.game_over()
        elif (snakeBoard[newHeadRow][newHeadCol] < 0):
            # eating food!  Yum!
            snakeBoard[newHeadRow][newHeadCol] = 1 + snakeBoard[headRow][headCol]
            self.canvas.data["headRow"] = newHeadRow
            self.canvas.data["headCol"] = newHeadCol
            self.colocar_comida()
        else:
            # normal move forward (not eating food)
            snakeBoard[newHeadRow][newHeadCol] = 1 + snakeBoard[headRow][headCol]
            self.canvas.data["headRow"] = newHeadRow
            self.canvas.data["headCol"] = newHeadCol
            self.remover_cola()

    def remover_cola(self):
        # find every snake cell and subtract 1 from it.  When we're done,
        # the old tail (which was 1) will become 0, so will not be part of the snake.
        # So the snake shrinks by 1 value, the tail.
        snakeBoard = self.canvas.data["snakeBoard"]
        rows = len(snakeBoard)
        cols = len(snakeBoard[0])
        for row in range(rows):
            for col in range(cols):
                if snakeBoard[row][col] > 0:
                    snakeBoard[row][col] -= 1

    def game_over(self):
        self.canvas.data["isGameOver"] = True

    def actualizar_cronometro(self):
        ignoreThisTimerEvent = self.canvas.data["ignoreNextTimerEvent"]
        self.canvas.data["ignoreNextTimerEvent"] = False
        if ((self.canvas.data["isGameOver"] == False) and
            (ignoreThisTimerEvent == False)):
            # only process timerFired if game is not over
            drow = self.canvas.data["snakeDrow"]
            dcol = self.canvas.data["snakeDcol"]
            self.mover_snake(drow, dcol)
            self.redibujar_todo()
        # whether or not game is over, call next timerFired
        # (or we'll never call timerFired again!)
        delay = 200 # milliseconds
        self.canvas.after(delay, self.actualizar_cronometro) # pause, then call timerFired again

    def redibujar_todo(self):
        self.canvas.delete(ALL)
        self.dibujar_tablero()
        if (self.canvas.data["isGameOver"] == True):
            cx = self.canvas.data["canvasWidth"]/2
            cy = self.canvas.data["canvasHeight"]/2
            self.canvas.create_text(cx, cy, text="Game Over!", font=("Helvetica", 32, "bold"))

    def dibujar_tablero(self):
        snakeBoard = self.canvas.data["snakeBoard"]
        rows = len(snakeBoard)
        cols = len(snakeBoard[0])
        for row in range(rows):
            for col in range(cols):
                self.drawSnakeCell(snakeBoard, row, col)

    def drawSnakeCell(self, snakeBoard, row, col):
        margin = self.canvas.data["margin"]
        cellSize = self.canvas.data["cellSize"]
        left = margin + col * cellSize
        right = left + cellSize
        top = margin + row * cellSize
        bottom = top + cellSize
        self.canvas.create_rectangle(left, top, right, bottom, fill="white")
        if (snakeBoard[row][col] > 0):
            # draw part of the snake body
            self.canvas.create_oval(left, top, right, bottom, fill="blue")
        elif (snakeBoard[row][col] < 0):
            # draw food
            self.canvas.create_oval(left, top, right, bottom, fill="green")
        # for debugging, draw the number in the cell
        if (self.canvas.data["inDebugMode"] == True):
            self.canvas.create_text(left+cellSize/2,top+cellSize/2,
                               text=str(snakeBoard[row][col]),font=("Helvatica", 14, "bold"))

    def cargar_tablero(self):
        rows = self.canvas.data["rows"]
        cols = self.canvas.data["cols"]
        snakeBoard = [ ]
        for row in range(rows): snakeBoard += [[0] * cols]
        snakeBoard[rows//2][cols//2] = 1
        self.canvas.data["snakeBoard"] = snakeBoard
        self.encontrar_cabeza_del_snake()
        self.colocar_comida()

    def colocar_comida(self):
        # place food (-1) in a random location on the snakeBoard, but
        # keep picking random locations until we find one that is not
        # part of the snake!
        snakeBoard = self.canvas.data["snakeBoard"]
        rows = len(snakeBoard)
        cols = len(snakeBoard[0])
        while True:
            row = random.randint(0,rows-1)
            col = random.randint(0,cols-1)
            if (snakeBoard[row][col] == 0):
                break
        snakeBoard[row][col] = -1

    def encontrar_cabeza_del_snake(self):
        # find where snakeBoard[row][col] is largest, and
        # store this location in headRow, headCol
        snakeBoard = self.canvas.data["snakeBoard"]
        rows = len(snakeBoard)
        cols = len(snakeBoard[0])
        headRow = 0
        headCol = 0
        for row in range(rows):
            for col in range(cols):
                if (snakeBoard[row][col] > snakeBoard[headRow][headCol]):
                    headRow = row
                    headCol = col
        self.canvas.data["headRow"] = headRow
        self.canvas.data["headCol"] = headCol

    def imprimir_instrucciones(self):
        print("Snake!")
        print("Use the arrow keys to move the snake.")
        print("Eat food to grow.")
        print("Stay on the board!")
        print("And don't crash into yourself!")
        print("Press 'd' for debug mode.")
        print("Press 'r' to restart.")

    def init(self):
        self.imprimir_instrucciones()
        self.cargar_tablero()
        self.canvas.data["inDebugMode"] = False
        self.canvas.data["isGameOver"] = False
        self.canvas.data["snakeDrow"] = 0
        self.canvas.data["snakeDcol"] = -1 # start moving left
        self.canvas.data["ignoreNextTimerEvent"] = False
        self.redibujar_todo()

app = SnakeGame()
app.run(16,16)
