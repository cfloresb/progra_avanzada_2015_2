"""
Similar a tkinter_2.py, pero empaquetamos los botones dentro de una grilla (Grid)
"""
import tkinter


class Ejemplo3(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
        # Creamos el marco, donde agruparemos los botones.
        # Como todos los objetos tkinter, es necesario crearlo pasando el objeto que lo contendrá al constructor de la clase.
        marco = tkinter.Frame(master=self)

        # Creamos los objetos que representan los controles.
        boton1 = tkinter.Button(master=marco, text='Botón 1')
        boton2 = tkinter.Button(master=marco, text='Botón 2')
        etiqueta = tkinter.Label(master=self, text='Esta es una etiqueta de texto')

        # Es posible empaquetar los objetos en una grilla a través del método grid():
        boton1.grid(row=0, column=0)
        boton2.grid(row=1, column=1)

        # Empaquetarmos los demás objetos dentro de nuestra aplicación
        # IMPORTANTE: No invocar el método pack() si el objeto ya está organizado (por ejemplo, se invocó grid()).
        marco.pack()
        etiqueta.pack()

        # Finalmente invocamos el loop de eventos principal:
        self.mainloop()


app = Ejemplo3()
