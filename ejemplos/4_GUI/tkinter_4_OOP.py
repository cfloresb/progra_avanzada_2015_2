"""
Ejemplo de formulario básico con tkinter
"""
import tkinter


class Ejemplo4(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
        # Creamos el marco, donde agruparemos los botones.
        # Como todos los objetos tkinter, es necesario crearlo pasando el objeto que lo contendrá al constructor de la clase.
        marco = tkinter.Frame(master=self)

        # Creamos los controles de nuestro formulario
        etiqueta_nombres = tkinter.Label(master=marco, text='Nombres')
        nombres = tkinter.StringVar()
        campo_nombres = tkinter.Entry(master=marco, textvariable=nombres)

        etiqueta_apellidos = tkinter.Label(master=marco, text='Apellidos')
        apellidos = tkinter.StringVar()
        campo_apellidos = tkinter.Entry(master=marco, textvariable=apellidos)

        boton = tkinter.Button(master=self, text='Guardar')

        # Empaquetamos los objetos en una grilla a través del método grid():
        etiqueta_nombres.grid(row=0, column=0)
        campo_nombres.grid(row=0, column=1)
        etiqueta_apellidos.grid(row=1, column=0)
        campo_apellidos.grid(row=1, column=1)

        # Empaquetamos los objetos principales dentro de nuestra aplicación
        marco.pack()
        boton.pack()

        # Es posible configurar el tamaño de los controles (Ancho x Alto).
        self.geometry('300x200')

        # El loop de eventos principal
        self.mainloop()

app = Ejemplo4()
