import pyfirmata

class ArduinoServo(pyfirmata.Arduino):
    def __init__(self, port):
        """Inicialización de la placa Arduino"""
        super().__init__(port)

        print("Estableciendo conexion con la placa ...")
        it = pyfirmata.util.Iterator(self)
        it.start()
        print("Conexion establecida!")

        self.pin_servo = self.digital[9]        # Pin de señal del servo (9)
        self.pin_servo.mode = pyfirmata.SERVO   # Configuración del pin servo
        self.mover_servo(90)                    # Posición/velocidad por defecto

        self.pin_potenciometro = self.get_pin('a:0:i')  # Configuración pin potenciómetro

    def mover_servo(self, angulo):
        self.pin_servo.write(angulo)

    def print_potenciometro(self):
        print(self.pin_potenciometro.read())

placa = ArduinoServo('/dev/ttyACM1')
