import pyfirmata
import time

board = pyfirmata.Arduino('/dev/ttyACM1')

print("Setting up the connection to the board ...")
it = pyfirmata.util.Iterator(board)
it.start()

button = board.get_pin('d:2:i')

while True:
    if not button.read():
        board.digital[3].write(True)
        board.digital[4].write(False)
        board.digital[5].write(False)

    else:
        board.digital[3].write(False)
        board.digital[4].write(False)
        board.digital[5].write(True)

        time.sleep(0.25)
        board.digital[4].write(True)
        board.digital[5].write(False)
        time.sleep(0.25)

board.exit()
