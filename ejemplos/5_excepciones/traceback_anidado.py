#Archivo traceback_anidado.py
'''Este archivo lanza una excepción no manejada
 y muestra un traceback anidado:'''

def lanza_excepcion():
    raise Exception("Hola! Esta es una excepcion!")

def main():
    lanza_excepcion()
    
if __name__ == '__main__':
    main()