'''
Created on 19-10-2014

@author: camilo
'''
'''
Para ejecutar un servidor SMTP dummy en la misma máquina,
y probar esta app, ejecutar en Unix (Linux o MacOS):
python3 -m smtpd -n -c DebuggingServer localhost:1025
'''

'''Importamos la biblioteca con facilidades para enviar mail a través del
protocolo SMTP (Simple Mail Transport Protocol)
http://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol'''
import smtplib 

#Solicitamos los datos necesarios al usuario
remitente = 'yo@aqui.com'
destinatario = input("Ingrese el destinatario: ")
mensaje = input("Ingrese el mensaje: ")

try:
    #Validamos la información ingresada:
    if len(destinatario) == 0 or len(mensaje) == 0:
        raise ValueError("La información ingresada no puede estar vacía")
    
    if '@' not in destinatario:
        #Estamos validando que la dirección tenga un @. 
        '''En realidad, validar un string como dirección de correo no es trivial.
         ver http://tools.ietf.org/html/rfc3696#section-4.3'''
        raise ValueError("La dirección de correo no es válida.")
    
    #Intentamos enviar el mail:
    server = smtplib.SMTP('localhost', 1025)
    server.sendmail(remitente, destinatario, mensaje)
    
    print("Mail enviado exitosamente :)")
    
except ValueError as error:
    #Lógica para manejar error en los datos ingresados:
    print("Ha ocurrido un error con la información ingresada. El detalle es:", error)
    
except ConnectionError as error:
    #Lógica para manejar algún error de conexión:
    print("Ha ocurrido un error de conexión. El detalle es: ", error)
    
except Exception as error:
    #Todos los demás tipos de excepción:
    print("Ha ocurrido un error. El detalle es:", error)

print("Programa finalizado.")