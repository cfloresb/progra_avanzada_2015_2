'''
Created on 19-10-2014

@author: camilo
'''

print("Bienvenido al programa para el cálculo de división.")
try:
    a = input("Ingrese numerador:")
    b = input("Ingrese el denominador:")
    numerador = int(a)
    denominador = int(b)    
    resultado = numerador / denominador
    print("El resultado de la división es:", resultado)

except ZeroDivisionError:
    print("Error, el denominador no puede ser cero.")
    
except ValueError:
    print("Sólo puede ingresar números.")
    
print("Fin del programa.")