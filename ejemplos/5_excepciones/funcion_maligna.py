'''
Created on 19-10-2014

@author: camilo
'''
#Archivo funcion_maligna.py
def funcion_maligna():
    try:
        while True:
            print("Tu computador está bajo mi control. Muajajajaja")
    except KeyboardInterrupt:
        '''Notar que, al capturarse la excepción, técnicamente para Python la función deja de ejecutarse
        por lo que no se provocará un desborde de pila si la llamamos nuevamente en este punto'''  
        funcion_maligna()
            
if __name__ == '__main__':
    funcion_maligna()