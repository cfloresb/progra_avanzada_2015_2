#Archivo excepcion_no_manejada.py
'''Este programa lanza una excepción.
 Si es llamada desde la línea de comando, 
como no hay ninguna cláusula try que la capture, 
el programa arrojará una traza (Traceback)'''
raise Exception("Esta es una excepcion no manejada")