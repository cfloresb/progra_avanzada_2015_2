'''
Created on 20-09-2014

@author: camilo
'''
'''Este programa pregunta el nombre de un archivo, 
luego lee dos números desde ese archivo y finalmente
calcula la división entre los números'''

nombre_archivo = input("Ingrese el nombre del archivo: ")

#Nos aseguraremos que el archivo existe:
import os
archivos = os.listdir() #Almacenamos el listado de archivos presentes
if nombre_archivo not in archivos:
    print("El archivo ingresado no existe!")

else:
    '''El nombre de archivo existe, continuamos...''' 
    archivo = open(nombre_archivo, 'r')
    numero1 = int(archivo.readline())
    numero2 = int(archivo.readline())
    archivo.close()
    resultado = numero1 / numero2
    
    print("El resultado de la división es ", resultado)