'''
Created on 20-09-2014

@author: camilo
'''
'''Este programa pregunta el nombre de un archivo, 
luego lee dos números desde ese archivo y finalmente
calcula la división entre los números'''

nombre_archivo = input("Ingrese el nombre del archivo: ")

#Nos aseguraremos que el archivo existe:
import os
archivos = os.listdir() #Almacenamos el listado de archivos presentes
if nombre_archivo not in archivos:
    print("Error: El archivo ingresado no existe!")

else:
    '''El nombre de archivo existe, continuamos...''' 
    archivo = open(nombre_archivo, 'r')
    
    '''Nos aseguramos que el archivo tenga al menos dos líneas'''
    total_lineas = sum(1 for linea in archivo)
    if total_lineas >= 2: 
        '''Como ya recorrimos el archivo tenemos que abrirlo nuevamente'''
        archivo.close()
        archivo = open(nombre_archivo, 'r') 
        numero1 = int(archivo.readline())
        numero2 = int(archivo.readline())
        archivo.close()
        resultado = numero1 / numero2
        
        print("El resultado de la división es ", resultado)
    else:
        print("Error: El archivo ingresado tiene menos de 2 líneas")