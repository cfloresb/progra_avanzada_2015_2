'''
Created on 20-09-2014

@author: camilo
'''
'''Este programa pregunta el nombre de un archivo, 
luego lee dos números desde ese archivo y finalmente
calcula la división entre los números'''

from validacion import es_numero

nombre_archivo = input("Ingrese el nombre del archivo: ")

#Nos aseguraremos que el archivo existe:
import os
archivos = os.listdir() #Almacenamos el listado de archivos presentes
if nombre_archivo not in archivos:
    print("Error: El archivo ingresado no existe!")

else:
    '''El nombre de archivo existe, continuamos...''' 
    archivo = open(nombre_archivo, 'r')
    
    '''Nos aseguramos que el archivo tenga al menos dos líneas'''
    total_lineas = sum(1 for linea in archivo)
    if total_lineas >= 2: 
        '''Como ya recorrimos el archivo tenemos que abrirlo nuevamente'''
        archivo.close()
        archivo = open(nombre_archivo, 'r')
        
        '''Tenemos que verificar que el contenido de cada línea pueda representarse como número'''
        linea1 = archivo.readline()
        linea2 = archivo.readline()
        archivo.close()
        
        #Comprobamos si los strings leídos son números (sin el salto de línea)
        if es_numero(linea1.rstrip('\n')) and es_numero(linea2.rstrip('\n')):
            numero1 = int(linea1)
            numero2 = int(linea2)
            '''Verificamos que el denominador no sea 0'''
            if numero2 != 0:                 
                resultado = numero1 / numero2        
                print("El resultado de la división es ", resultado)
            else:
                print("Error: El denominador no puede ser 0")
        else:
            print("Error: El archivo no contiene sólo números")
    else:
        print("Error: El archivo ingresado tiene menos de 2 líneas")