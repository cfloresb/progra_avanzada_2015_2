#archivo clausula_finally.py
def sin_finally():
    try:
        archivo = open('archivo.txt', 'a')
        archivo.write("Hola, este es un ejemplo del uso de finally y excepciones")
        #Supongamos que ahora queremos ejecutar un cálculo y escribir el resultado en el archivo 
        archivo.write(2/0)
        archivo.close()
    except Exception as error:
        print("Ha ocurrido un error. El detalle es el siguiente:")
        print(error)
    print("Fin del programa")
    
    
def con_finally():
    try:
        archivo = open('archivo.txt', 'a')
        archivo.write("Hola, este es un ejemplo del uso de finally y excepciones")
        #Supongamos que ahora queremos ejecutar un cálculo y escribir el resultado en el archivo 
        archivo.write(2/0)        
    except Exception as error:
        print("Ha ocurrido un error. El detalle es el siguiente:")
        print(error)
    finally:
        #El código que esté en el finally se ejecutará siempre, haya o no excepcion, y aunque no se capture:
        archivo.close()
        print("Fin del programa")