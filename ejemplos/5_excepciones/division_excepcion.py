'''
Created on 20-09-2014

@author: camilo
'''

def calcular_division():
    try:
        nombre_archivo = input("Ingrese el nombre del archivo: ")
        archivo = open(nombre_archivo, 'r')
        numero1 = int(archivo.readline())
        numero2 = int(archivo.readline())
        archivo.close()
        
        resultado = numero1 / numero2
        
        print("El resultado de la división es ", resultado)
        
    except Exception as e:
        # Capturamos la excepción e imprimimos su descripción.
        # Notar que la excepción se puede acceder en este trozo de
        # código a través de la variable e
        print("Ha ocurrido un error. El detalle es el siguiente:", e)
        
        '''Ya que "salimos" del código en el 'try' (ya no estamos en la función),
         volvemos a llamarla hasta que no haya error'''        
        calcular_division()
        
if __name__ == '__main__':
    calcular_division()