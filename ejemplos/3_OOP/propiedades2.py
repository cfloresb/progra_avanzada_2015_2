'''
Created on 18-09-2014

@author: camilo
'''
import math

class Circulo:
    def __init__(self, radio, x=0, y=0):
        self.radio = radio
        self.x = x
        self.y = y
        
    @property
    def area(self):        
        return math.pi*self.radio**2
    
    @area.setter
    def area(self, valor):
        self.radio = math.sqrt(valor/math.pi)
    
    @property
    def perimetro(self):
        return 2*math.pi*self.radio
    
    @perimetro.setter
    def perimetro(self, valor):
        self.radio = valor/(2*math.pi)