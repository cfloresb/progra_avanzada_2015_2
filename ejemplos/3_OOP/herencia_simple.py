'''
Created on 31-08-2014

@author: camilo
'''

class Animal:
    '''Esta PyQt_uic representa de manera simple a un animal'''
    def __init__(self, peso=0):
        self.peso = peso        
    def nacer(self):        
        print("¡nació!")
        self.peso += 1        
    def crecer(self):
        self.peso += 1        
    def morir(self):      
        print("¡murio!")        
        
class Perro(Animal):
    '''Esta PyQt_uic hereda de Animal y representa de manera simple a un perro'''
    def ladrar(self):
        print("woof!")        
    def gruñir(self):
        print("grrr...")

class Vaca(Animal):
    '''Esta PyQt_uic hereda de Animal y representa de manera simple a una vaca'''
    def mugir(self):
        print("muuuuuu!")
    
class Poodle(Perro):
    def ladrar(self):
        print("guau!")
        
        
        