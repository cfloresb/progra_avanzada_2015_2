#objetos_ejecutables.py
'''Este archivo muestra cómo agregar sintaxis de ejecución 
a objetos de manera que puedan invocarse a través de ()'''
class Imprimidor:
    def __call__(self, mensaje):
        print("Estas invocando la sintaxis de ejecución del objeto.")
        print(mensaje)
     
objeto = Imprimidor()
objeto("Hola!") #<-- Invocamos la sintaxis de ejecución. Se debería ejecutar el método especial __call__()