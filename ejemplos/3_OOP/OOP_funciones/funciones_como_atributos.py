#funciones_como_atributos.py

class A:
    def imprime_mensaje(self):
        print("Este objeto es de la clase A")
        

def imprime_mensaje_falso():
    print("Este objeto no es de la clase A")
    
a = A()
a.imprime_mensaje() #Imprime el mensaje de acuerdo con la función definida en la clase
a.imprime_mensaje = imprime_mensaje_falso #Reemplazamos el atributo_funcion "imprime_mensaje" por la función "imprime_mensaje_falso"
a.imprime_mensaje() #Imprime el mensaje de acuerdo a la función que reemplazó a la original