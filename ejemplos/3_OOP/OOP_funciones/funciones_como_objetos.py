#funciones_como_objetos.py
'''Este ejemplo muestra cómo a las funciones es posible agregarles atributos
 y ser pasadas como parámetros a otras funciones.
 ¡Las funciones también son objetos!'''

def funcion1():
    print("La función 1 ha sido llamada")
    
def funcion2():
    print("La función 2 ha sido llamada")
    
def otra_funcion(funcion):
    print("La descripción de la función es: ", funcion.descripcion)
    print("El nombre de la función es: ", funcion.__name__) #preguntamos por el atributo privado __name__ de los objetos de la clase function
    print("La clase de la función es: ", funcion.__class__) #preguntamos a qué clase pertenecen el objeto pasado
    print("Ahora vamos a llamar a la función pasada como parámetro:")
    funcion()   #Ocupamos la sintaxis de invocación sobre el objeto funcion:los paréntesis ()
    
funcion1.descripcion = "Esta es la función 1, que es bastante inútil..." #Agregamos el atributo descripción al objeto funcion1
funcion2.descripcion = "Esta es la función 2, que es más inútil..." #Idem para funcion2
otra_funcion(funcion1)  #En este caso, pasamos el objeto funcion1 (la funcion1 definida en línea 10) a la función otra_funcion
otra_funcion(funcion2)  #Lo mismo, pero con la funcion2