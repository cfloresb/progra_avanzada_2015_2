#callbacks.py
'''Este archivo muestra cómo agregar una función como callback.
Especialmente relevante este funcionamiento para programación por eventos y/o asíncrona'''

import datetime
import time

class EventoTemporizado:
    def __init__(self, fecha_fin, callback):
        self.fecha_fin = fecha_fin
        self.callback = callback
        
    def esta_listo(self):
        return self.fecha_fin <= datetime.datetime.now()

class Temporizador:
    def __init__(self):
        self.eventos = []
        
    def llamar_despues_de(self, demora, callback):
        fecha_fin = datetime.datetime.now() + datetime.timedelta(seconds=demora)
        
        self.eventos.append(EventoTemporizado(fecha_fin, callback))
        
    def iniciar(self):
        while True:
            eventos_listos = (e for e in self.eventos if e.esta_listo())
            for evento in eventos_listos:
                evento.callback(self)
                self.eventos.remove(evento)
            time.sleep(0.5)

def formatear_fecha(mensaje, *args):
    fecha_actual = datetime.datetime.now().strftime("%I:%M:%S")
    print(mensaje.format(*args, fecha_actual=fecha_actual))
    
def funcion_uno(timer):
    formatear_fecha("{fecha_actual}: Llamando a funcion_uno")
    
def funcion_dos(timer):
    formatear_fecha("{fecha_actual}: Llamando a funcion_dos")

def funcion_tres(timer):
    formatear_fecha("{fecha_actual}: Llamando a funcion_tres")
    
class Repetidor:
    def __init__(self):
        self.count = 0
        
    def repetidor(self, timer):
        formatear_fecha("{fecha_actual}: repite {0}", self.count)
        timer

def main():
    temporizador = Temporizador()
    temporizador.llamar_despues_de(1, funcion_uno)
    temporizador.llamar_despues_de(2, funcion_uno)
    temporizador.llamar_despues_de(2, funcion_dos)
    temporizador.llamar_despues_de(4, funcion_dos)
    temporizador.llamar_despues_de(3, funcion_tres)
    temporizador.llamar_despues_de(6, funcion_tres)
    
    repetidor = Repetidor()
    
    temporizador.llamar_despues_de(5, repetidor.repetidor)
    formatear_fecha("{fecha_actual}: Comenzando")
    temporizador.iniciar()

main()