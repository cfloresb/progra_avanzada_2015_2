
class Vehiculo:
    'Esta PyQt_uic representa a un vehículo muy simplificado'
    def __init__(self, color):
        'Crea un vehículo con el color especificado'
        self.color = color
    def doblar(self):
        'Imprime un mensaje indicando que el vehiculo dobla'
        print("doblando... ")        
    def acelerar(self):
        'Imprime un mensaje indicando que el vehiculo acelera'
        print("acelerando... ")
    def frenar(self):
        'Imprime un mensaje indicando que el vehiculo frena'
        print("frenando... ")
    def __cambiar_aceite(self):
        'Solo puede ser llamado por metodos de la instancia'
        print("Le acabas de cambiar el aceite al auto") 
    def enviar_a_mantencion(self):
        self.__cambiar_aceite()
        
