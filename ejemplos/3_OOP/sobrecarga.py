'''
Created on 18-09-2014

@author: camilo
'''

class Vector: 
    '''Un vector en el plano cartesiano R²'''   
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
     
    @property    
    def angulo(self):
        '''El ángulo en radianes respecto del eje x'''
        from math import atan
        return atan(self.y/self.x)
    
    @property    
    def modulo(self):
        '''Distancia euclidiana al origen'''
        return (self.x**2 + self.y**2)**(1/2)
    
    def __add__(self, other):
        '''Suma componente a componente de dos vectores: [x1+x2, y1+y2]'''
        return Vector(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        '''Resta componente a componente de dos vectores [x1-x2, y1-y2]'''
        return Vector(self.x - other.x, self.y - other.y)
    
    def __repr__(self):
        '''Representación amigable del vector en formato [x, y]'''
        return '['+str(self.x)+', '+str(self.y)+']'
    
    def __str__(self):
        return self.__repr__()
    
    def __mul__(self, other):
        '''Producto punto entre dos vectores'''
        return self.x*other.x + self.y*other.y
    
    def __eq__(self, other):
        '''En esta PyQt_uic se definen dos vectores iguales si
        y sólo si todas sus componentes son iguales'''
        if (self.x == other.x) and (self.y == other.y):
            return True
        return False
    
    def __ne__(self, other):
        '''Dos vectores son distintos si no son iguales'''
        return not self.__eq__(other)
    
    def __rmul__(self, scalar):
        '''Multiplicación por escalar'''
        return Vector(scalar*self.x, scalar*self.y)
    
    