'''
Created on 19-09-2014

@author: camilo
'''

class Complejo:
    '''Representación sencilla de complejo, compatible con Vector
    Sólo para demostración, NO USAR, Python incorpora el tipo 
    Complejo dentro de sus tipos de datos básicos'''
    def __init__(self, re=0, im=0):
        self.re = re
        self.im = im
        
    @property
    def x(self):
        '''Para compatibilidad con Vector'''
        return self.re
    
    @property
    def y(self):
        '''Para compatibilidad con Vector'''
        return self.im
    
    def __mul__(self, other):
        '''Multiplicación de dos números complejos:
        (a+bi)*(c+di) = (ac-bdi)'''
        return Complejo(self.re*other.re, - self.im*other.im)
    
    def __repr__(self):
        if self.im < 0:
            signo = ''
        else:
            signo = '+'
        return '('+str(self.re) + signo +str(self.im) + 'i)'
    
    def __truediv__(self, other):
        '''División de dos numeros complejos:
        (a+bi)/(c+di) = ((ac+bd)+(bc-ad)i)/(c²+d²)'''
        denominador = other.re**2 + other.im**2
        parte_re = (self.re*other.re+self.im*other.im)/denominador
        parte_im = (other.im*self.re - self.re*other.im)/denominador
        return Complejo(parte_re, parte_im)
    