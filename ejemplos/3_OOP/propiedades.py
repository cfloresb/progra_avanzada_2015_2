'''
Created on 18-09-2014

@author: camilo
'''
import math

class Circulo:
    def __init__(self, radio, x, y):
        self.radio = radio
        self.x = x
        self.y = y
        
    @property
    def area(self):        
        return math.pi*self.radio**2
    
    @property
    def perimetro(self):
        return 2*math.pi*self.radio     
