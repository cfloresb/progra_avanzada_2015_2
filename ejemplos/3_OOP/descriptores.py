'''
Created on 21-09-2014

@author: camilo
'''
class TypedProperty(object):
    def __init__(self, name, type, default=None):
        self.name = "_" + name
        self.type = type
        self.default = type() if default is None else default
    def __get__(self, instance, cls):
        return getattr(instance, self.name, self.default) if instance else self
    def __set__(self, instance, value):
        if not isinstance(value, self.type):
            raise TypeError("Must be a %s" % self.type)
        setattr(instance, self.name, value)
    def __delete__(self, instance):
        raise AttributeError("Can't delete attribute")