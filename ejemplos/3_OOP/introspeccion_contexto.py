'''
Created on 20-09-2014

@author: camilo
'''
def funcion(parametro1, parametro2=0):
    return vars()

if __name__ == '__main__':
    contexto_funcion = funcion(45, 'este es el valor del parámetro 2')
    print('El contexto dentro de la funcion es:' + str(contexto_funcion))