'''
Created on 20-09-2014

@author: camilo
'''
from sobrecarga import Vector
from polimorfismo import Complejo

def sumar1(objeto):
    atributos = vars(objeto)
    for nombre_atributo, valor in atributos.items():
        if isinstance(valor, (int, float)):            
            codigo = 'objeto.'+ nombre_atributo + ' += 1 '
            exec(codigo)
            #Las dos lineas anteriores se pueden reemplazar por
            # setattr(objeto, nombre_atributo, valor+1)
    return objeto

if __name__ == '__main__':
    vector1 = Vector(1,1)
    complejo1 = Complejo(2,2)
    print("Valores de la variables antes de llamar a la función sumar1():")
    print("vector1: "+ str(vector1))
    print("complejo1:" + str(complejo1))
    
    v1 = sumar1(vector1)
    c1 = sumar1(complejo1)
    
    print("\nValores de las variables después de llamar a la funcion sumar1()")
    print("v1: " + str(v1))
    print("vector1: " + str(vector1))
    print("c1: " + str(c1))
    print("complejo1: " + str(complejo1))
    