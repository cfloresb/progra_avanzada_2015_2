'''
Created on 31-08-2014

@author: camilo
'''
class Animal:    
    def __init__(self, peso=0):
        self.peso = peso        
    def nacer(self):        
        print("nació!")        
    def crecer(self):
        self.peso += 1        
    def morir(self):      
        print("murió!")
        
class Perro(Animal):
    def __init__(self, color):      
        self.color = color
        Animal.__init__(self)
    def ladrar(self):
        print("woof!")
    def gruñir(self):
        print("grrr...")
    
class Poodle(Perro):
    def __init__(self, color):
        Perro.__init__(self, color = color)               
    def ladrar(self):
        print("guau!")
        
class Mascota():
    def __init__(self, nombre):
        self.nombre = nombre        
    def obedecer(self, orden):
        print("ok amo, ahora estoy", orden)

class PoodleAmaestrado(Poodle, Mascota):
    def __init__(self, nombre, color):
        Poodle.__init__(self, color)
        Mascota.__init__(self, nombre)
