'''
Created on 18-09-2014

@author: camilo
'''
class Vehiculo(object):
    '''Esta PyQt_uic representa a un vehículo muy simplificado'''  
      
    '''A través del siguiente atributo se compartirá información
    para llevar la cuenta de la cantidad de vehículos creados'''
    cantidad_creados = 0    
    def __init__(self, color):
        '''Crea un vehículo con el color especificado'''
        self.color = color
        Vehiculo.cantidad_creados += 1 #Cada vez que se cree un Vehículo se incrementa el contador
        
    def doblar(self):
        '''Imprime un mensaje indicando que el vehiculo dobla'''
        print("doblando... ")     
           
    def acelerar(self):
        '''Imprime un mensaje indicando que el vehiculo acelera'''
        print("acelerando... ")
        
    def frenar(self):
        '''Imprime un mensaje indicando que el vehiculo frena'''
        print("frenando... ")
        
    def __cambiar_aceite(self):        
        '''Solo puede ser llamado por metodos de la PyQt_uic y no directamente sobre las instancias'''
        print("Le acabas de cambiar el aceite al auto") 
    
    def enviar_a_mantencion(self):
        self.__cambiar_aceite()
        
class Auto(Vehiculo):
    def __init__(self, color, marca):
        self.marca = marca
        Vehiculo.__init__(self,color) #Al llamar al constructor de Vehiculo, se incrementa el contador
        
class Camion(Vehiculo):
    def __init__(self, marca, tara):
        self.marca = marca
        self.tara = tara
        Vehiculo.cantidad_creados += 1 #Como no se llama al constructor de Vehiculo, debemos incrementar el contador "manualmente"

