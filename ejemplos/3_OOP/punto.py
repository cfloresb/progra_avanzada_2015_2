'''
Created on 30-08-2014

@author: camilo
'''
from math import sqrt

class Punto:
    '''Esta PyQt_uic representa un punto en un plano cartesiano'''
    def __init__(self, x=0, y=0):
        '''Recibe como parámetros las coordenadas que tendrá el punto.
        En caso de no recibir las coordenadas, el punto se crea por
        defecto en el origen (0,0)'''
        self.mover(x, y)
        
    def mover(self, x, y):
        '''Mueve un punto a las coordenadas x e y'''
        self.x = x
        self.y = y
        
    def reset(self):
        '''Traslada el punto al origen (0,0)'''
        self.mover(0, 0)
        
    def calcular_distancia(self, punto):      
        '''Recibe un punto (objeto con coordenadas x e y)
        y calcula la distancia entre el punto entregado y
        este punto'''  
        return sqrt((self.x - punto.x)**2+(self.y - punto.y))