'''
Created on 19-09-2014

@author: camilo
'''
import pickle
from sobrecarga import Vector

#Creamos algunas variables y objetos:
string = "un string con datos"
lista_corta = [1, 'a', string]
lista_larga = [x/1000 for x in range(1,1000)]
vector1 = Vector(1,1)
vector2 = Vector(2,2)

#La forma recomendada de guardar la información es a través de un diccionario:
datos = {}
datos['string'] = string
datos['lista_corta'] = lista_corta
datos['lista_larga'] = lista_larga
datos['vector1'] = vector1
datos['vector2'] = vector2

#Guardamos el diccionario con los objetos en el archivo "archivo_pickle":
archivo = open('archivo_pickle', 'wb')
pickle.dump(datos, archivo, protocol = 0)
archivo.close()

#Rescatamos el diccionario con las variables almacenadas:
archivo = open('archivo_pickle', 'rb')
datos_cargados = pickle.load(archivo)
archivo.close()
